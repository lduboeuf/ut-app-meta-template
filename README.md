# Ubuntu Touch App Template

A template to generate apps for Ubuntu Touch.

## Usage

To get started with this template, simply run `clickable create`.

### OR

* Install the needed tools
    * [Cookiecutter](https://cookiecutter.readthedocs.io/en/latest/)
    * [clickable](https://clickable-ut.dev/en/latest/)
* Generate the project
    * Run `cookiecutter gl:clickable/ut-app-meta-template`
* Go into the directory for your new project
* Run `clickable` to compile and install your new app!

## License

Copyright (C) 2020 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
